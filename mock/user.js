export const MOCK_USER_BOOKMARKS = [
  { id: 1, placeId: 1 },
  { id: 2, placeId: 2 },
  { id: 3, placeId: 11 },
  { id: 4, placeId: 12 },
  { id: 5, placeId: 18 },
  { id: 6, placeId: 20 },
  { id: 7, placeId: 25 },
];
