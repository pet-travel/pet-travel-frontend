module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        app: {
          yellow: "#F6C844",
          blue: "#8DACDF",
          "blue-light": "#AFCDFF"
        },
      },
    },
  },
  plugins: [],
  mode: "jit",
};
