import React from "react";

const Footer = () => {
  return (
    <footer className="flex justify-between py-12 px-24">
      <p className="text-sm">
        @ 2022 All rights reserved. Proudly made in Phayao
      </p>
      <div>
        <h3 className="mb-3">หน้าหลัก</h3>
        <ul className="list-disc space-y-3">
          <li>รายการโปรด</li>
          <li>สถานที่ยอดนิยม</li>
          <li>สถานที่ท่องเที่ยวภาคเหนือ</li>
        </ul>
      </div>
      <div>
        <h4>PetTravel.com </h4>
      </div>
      <div>
        <h3>ติดต่อเรา</h3>
      </div>
    </footer>
  );
};

export default Footer;
