import React, { useState, useEffect } from 'react';
import Image from "next/image";
import Link from "next/link";
import axios from "axios";


const NavbarPlaceOwner = () => {
  const [locations, setLocation] = useState([])
  useEffect(() => {

    axios.get(`http://localhost:3001/locations`).then((res) => {
      console.log(res.data);
      setLocation(res.data)
    })
  }, [])

  const loginData = () => {
    if (process.browser) {
      if (localStorage.getItem("token")) {
        return localStorage.getItem("name");
      }
    } else {
      return false;
    }
  };

  const logoutfunction = () => {
    localStorage.removeItem('token');
    location.href = "/"
  };

  return (
    <nav className="flex justify-between p-6 bg-app-yellow shadow-lgs">
      <h1 className="font-[Roboto,sans-serif] font-black italic text-3xl text-white">
        Pet Travel
      </h1>

      <div className="relative">
        <div className="focus:outline-none pl-3 pr-12 py-2 rounded-lg w-96 shadow-md bg-white">
          <details className="ml-2 relative bg-white text-gray-600 cursor-pointer z-50 group">

            <summary className="before:hidden group-open:before:block before:contents-[' '] before:cursor-default before:h-screen before:w-screen before:fixed before:top-0 before:right-0">เลือกจังหวัด</summary>
            <div className="z-50 h-60 bg-white absolute top-0 left-0 w-full">
              <div className="ml-5">
                {locations.map(e => <Link key={e.id} href={`/location/${e._id}`}>
                  <div className=" text-black">{e.name}</div>
                </Link>)}
              </div>
            </div>
          </details>
        </div>
      </div>

      <div className="flex items-center text-white">
        <Link href="/">
          <a className="flex flex-col px-6">
            <Image src="/home.svg" height={24} width={24} />
            <span className="text-xs mt-1">หน้าหลัก</span>
          </a>
        </Link>

        <Link href="/profile">
          <div className="border-r border-l flex flex-col px-6">
            <Image src="/bookmark.svg" height={24} width={24} />
            <span className="text-xs mt-1">รายการโปรด</span>
          </div>
        </Link>

        <Link href="/manage_place">
          <div className="border-r border-l flex flex-col px-6">
            <Image src="/myplace.svg" height={24} width={24} />
            <span className="text-xs mt-1">สถานที่ของฉัน</span>
          </div>
        </Link>

        <Link href="/add_place">
          <div className="border-r border-l flex flex-col px-6">
            <Image src="/addplace.svg" height={24} width={24} />
            <span className="text-xs mt-1">เพิ่มสถานที่</span>
          </div>
        </Link>

        {loginData() ? (
          <div className="flex flex-col ">
            <Image src="/user.svg" height={32} width={32} />
            <span className="ml-3">{loginData()}</span>
            <div className="select-none flex justify-center cursor-pointer hover:bg-red-500" onClick={logoutfunction}>
              Logout
            </div>
          </div>
        ) : (
          <div>
            <Link href="/login">
              <div className="flex px-6">
                <Image src="/user.svg" height={32} width={32} />
                <span className="ml-2">เข้าสู่ระบบ</span>
              </div>
            </Link>
          </div>
        )}
      </div>
    </nav>
  );
};

export default NavbarPlaceOwner;
