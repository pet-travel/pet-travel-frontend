import Navbar_place_owner from "../../components/Navbar_place_owner";
import Image from "next/image";
import React, { useState, useEffect } from 'react';
import axios from "axios";
import { useRouter } from "next/router";
import { create } from 'ipfs-http-client'
const ipfs = create({
    host: 'ipfs.infura.io',
    port: 5001,
    protocol: 'https',
})


const EditPlace = () => {
    const images2 = "/pet-travel-dog.png";
    const [name, setname] = useState("");
    const [district, setdistrict] = useState("");
    const [location, setlocation] = useState("");
    const [detail, setdetail] = useState("");
    const [img, setimg] = useState("");
    const [imgs, setimgs] = useState("");
    const router = useRouter()
    const [locations, setLocation] = useState([])
    const [place, setPlace] = useState({})
    useEffect(() => {

        axios.get(`http://localhost:3001/locations`).then((res) => {
            console.log(res.data);
            setLocation(res.data)
        })
    }, [])

    useEffect(() => {
        if (router.query.id) {
            axios.get(`http://localhost:3001/places/${router.query.id}`).then(res => {
                setPlace(res.data)
                setdistrict(res.data.district);
                setProvince(res.data.locationId)
                setname(res.data.name);
                setdetail(res.data.detail)
                setimg(res.data.img)
            })
        }
    }, [router.query.id])

    const [province, setProvince] = useState("")

    const editplace = async () => {
        console.log(name, district, location, detail, img, imgs);
        const res = await axios.patch(`http://localhost:3001/places/${router.query.id}`, { ...place, name: name, district: district, locationId: province, detail: detail, img: img, imgs: imgs }, {
            headers: {
                authorization: `Bearer ${localStorage.getItem("token")}`
            }
        })
        if (res.data == "ADD PLACE FAILED") {
            alert("ADD PLACE FAILED")
        }
        else {
            router.push("/manage_place")
        }
    }

    const back = async () => {
        router.push("/manage_place")
    }

    const handleFileChange = async (e) => {
        if (e.target.files[0]) {
            try {
                const { cid } = await ipfs.add({
                    path: '/',
                    content: e.target.files[0],
                })
                const url = `https://ipfs.infura.io/ipfs/${cid}`
                setimg(url)
            } catch (error) {
                console.log('Error uploading file: ', error)
            }
        }
    }

    return <div>
        <Navbar_place_owner />
        <div className="relative w-screen h-[70vh]">
            <div className="absolute right-0">
                <Image src={images2} height={350} width={500} objectFit="contain" />
            </div>
            <div className="flex p-14">
                <div className="space-y-10 w-full relative">
                    <div className="h-16 w-60 bg-[#AFCDFF] rounded-md text-3xl flex items-center justify-center ">
                        <p>เพิ่มสถานที่</p>
                    </div>
                    <div className="w-2/3 grid grid-cols-2 gap-6">
                        <div className="text-3xl w-full items-center flex-col space-y-5">
                            <div className="space-y-3">
                                <p>ชื่อสถานที่</p>
                                <input value={name} onChange={(e) => setname(e.target.value)} className="h-10 w-full bg-[#AFCDFF] rounded-md ">
                                </input>
                            </div>
                            <div className="space-y-3">
                                <p>จังหวัด</p>
                                <select value={province} onChange={(e) => setProvince(e.target.value)}>
                                    {locations.map(e => <option value={e._id}>{e.name}</option>)}
                                </select>
                            </div>
                        </div>


                        <div className="text-3xl w-full items-center flex-col space-y-5">
                            <div className="space-y-3">
                                <p>เลขที่ หมู่บ้าน ตำบล อำเภอ</p>
                                <input value={district} onChange={(e) => setdistrict(e.target.value)} className="h-10 w-full bg-[#AFCDFF] rounded-md">
                                </input>
                            </div>
                        </div>

                        <div className="col-span-2 space-y-3">
                            <div className="space-y-5 text-3xl">
                                <p>คำอธิบายสถานที่และข้อมูลติดต่อ</p>
                            </div>
                            <textarea value={detail} onChange={(e) => setdetail(e.target.value)} className="resize-none h-[361px] w-full bg-[#AFCDFF] rounded-md text-3xl">
                            </textarea>
                        </div>
                    </div>
                    
                    <div className="h-[150px] w-full bg-[#AFCDFF] rounded-md">
                        <div className="px-4 py-4 text-3xl">
                            <Image src="/addimg.svg" height={32} width={32} />
                            <p className="">เพิ่มรูปภาพ</p>
                        </div>
                        <input type="file" className="addimg , h-[50px]" onClick={(event) => { event.target.value = null }} onChange={handleFileChange} />
                        {img && (
                            <div className="flex justify-center my-6 p-6 flex-col h-[600px] w-[600px] items-center">
                                <h2>Previwe</h2>
                                <img src={img} className="h-[30rem]" alt="" />
                            </div>
                        )}
                        <div className="w-96 h-20 absolute right-0 bottom-0 flex justify-around items-center ">

                            <div className="bg-red-500 px-8 py-4 rounded-md cursor-pointer">
                                <button onClick={back} className="text-white p-2 rounded-md">CANCEL</button>
                            </div>

                            <div className="bg-green-400 px-8 py-4 rounded-md cursor-pointer">
                                <button onClick={editplace} className="text-white p-2 rounded-md">SAVE</button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
}


export default EditPlace