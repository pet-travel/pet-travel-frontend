import Navbar_place_owner from "../components/Navbar_place_owner";
import Image from "next/image";
import Footer from "../components/Footer";
import React, { useState, useEffect } from 'react';
import axios from "axios";
import { useRouter } from "next/router";

export default function edit_profile() {
    const images2 = "/pet-travel-dog.png";

    const [name, setname] = useState("");
    const [nickname, setnickname] = useState("");
    const [email, setemail] = useState("");
    const [phone, setephone] = useState("");
    const router = useRouter()
    
    const editprofile = async () => {
        console.log(name, nickname, email, phone);
        const res = await axios.patch("http://localhost:3001/users", { name: name, nickname: nickname, email: email, phone: phone})
        if (res.data == "EDIT PROFILE FAILED") {
            alert("EDIT PROFILE FAILED")
        }
        else {
            router.push("/profile")
        }
        
    
    const backpage = ()=> {
        router.push("/profile")
    }

    }
    return (
        <div>
            <Navbar_place_owner />
            <div>
                <div className="absolute right-0">
                    <Image src={images2} height={350} width={500} objectFit="contain" />
                </div>
                <div className="flex p-14">
                    <div className="w-4/12 space-y-10 flex items-center flex-col">
                        <div className="h-52 w-52 bg-black rounded-full">

                        </div>
                    </div>
                    <div className="w-8/12 space-y-6 text-xl mt-14">
                        <h1>My Profile</h1>
                    </div>
                </div>
                <div className="w-full flex justify-center">
                    <div className="w-2/3 grid grid-cols-2 gap-6 ">
                        <div className="text-2xl w-full items-center flex-col space-y-5">
                            <div className="space-y-3">
                                <p>Username</p>
                                <input value={name} onChange={(e) => setname(e.target.value)} className="h-10 w-full bg-[#AFCDFF] rounded-md ">
                                </input>
                            </div>
                            <div className="space-y-3">
                                <p>Nickname</p>
                                <input value={nickname} onChange={(e) => setname(e.target.value)} className="h-10 w-full bg-[#AFCDFF] rounded-md ">
                                </input>
                            </div>
                            <div className="space-y-3">
                                <p>Email</p>
                                <input value={email} onChange={(e) => setname(e.target.value)} className="h-10 w-full bg-[#AFCDFF] rounded-md">
                                </input>

                            </div>
                            <div className="space-y-3">
                                <p>Phone number</p>
                                <input value={phone} onChange={(e) => setname(e.target.value)} className="h-10 w-full bg-[#AFCDFF] rounded-md">
                                </input>

                            </div>

                        </div>
                    </div>
                </div>
                <div className="w-[1450px] h-20 flex justify-end items-center">
                    <div className="bg-green-400 px-8 py-4 rounded-md cursor-pointer">
                        <button onClick={editprofile} className="text-white p-2 rounded-md">SAVE</button>
                    </div>
                </div>
                <div className="w-[1450px] h-20 flex justify-end items-center">
                    <div className="bg-red-400 px-8 py-4 rounded-md cursor-pointer">
                        <button className="text-white p-2 rounded-md">CANCEL</button>
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}