import React, { useEffect, useState } from 'react'
import axios from 'axios'


const showAPI = () => {
  const [data, setData] = useState([])
  useEffect(() => {
    axios.get(`http://localhost:3001/places`).then((res) => {
      console.log(res.data);
      setData(res.data)
    })
  }, [])

  return (
    <div>
      {data.map((e) => {
        return <div>
          <div><h1>{e.name}</h1></div>
          <div>{e.location}</div>
          <div>{e.district}</div>
          <div>{e.detail}</div>
          <div>{e.img}</div>
        </div>
      })}
    </div>
  )
}


export default showAPI