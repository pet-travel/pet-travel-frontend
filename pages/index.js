import Image from "next/image";
import Navbar from "../components/Navbar";
import NavbarPlaceOwner from "../components/Navbar_place_owner";
import Footer from "../components/Footer";
import Link from "next/link";
import { MOCK_LOCATIONS } from "../mock/location";
import React, { useEffect, useState } from 'react'
import axios from 'axios'


export default function Home() {
  const [locations, setLocation] = useState([])
  const [nav, setNav] = useState(false)
  useEffect(() => {
    axios.get(`http://localhost:3001/locations`).then((res) => {

      setLocation(res.data)
    })
    axios.get(`http://localhost:3001/users/me`, {
      headers: {
        authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    }).then((res) => {

      if (res.data.role == "place_owner") {
        setNav(true)
      } else { setNav(false) }
    })
  }, [])
  const images1 = ["/11.jpg"];
  const images2 = ["/1.jpg", "/2.jpg", "/3.jpg"];


  return (
    <div>
      {nav ? <NavbarPlaceOwner /> : <Navbar />}
      <div className="relative w-screen h-[70vh]">
        <Image src={images1[0]} className="" layout="fill" objectFit="cover" />
      </div>

      {/* <div className="py-3 my-3 px-24">
        <h2 className="font-bold ml-6">สถานที่ยอดนิยม</h2>

        <div className="flex px-6 mt-8 justify-between">
          {images2.map((e, i) => (
            <div
              key={i}
              className="w-80 h-56 rounded-md overflow-hidden shadow-lg relative"
            >
              <Image src={e} className="" layout="fill" objectFit="cover" />
            </div>
          ))}
        </div>
      </div> */}

      <div className="bg-app-blue text-white py-12 px-24">
        <h2 className="font-bold ml-6 mb-6">สถานที่ท่องเที่ยวในภาคเหนือ</h2>
        <div className="grid grid-cols-3 px-12 gap-6">
          {locations.map((e) => (
            <Link key={e.name} href={`/location/${e._id}`}>
              <a>
                <p className="mb-2 font-semibold">{e.name}</p>
                <div className="relative w-full h-48 rounded-lg shadow-lg overflow-hidden">
                  <Image src={e.img} objectFit="cover" layout="fill" />
                </div>
              </a>
            </Link>
          ))}
        </div>
      </div>

      <Footer />
    </div>
  );
}
