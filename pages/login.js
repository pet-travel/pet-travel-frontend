import Image from "next/image"
import React, { useState , useEffect} from 'react';
import axios from "axios";
import {useRouter} from "next/router";


export default function login() {
    const [email, setemail] = useState("");
    const [password, setpassword] = useState("");
    const router = useRouter()
    useEffect(()=>{
        if(localStorage.getItem("token")){router.push("/")}
    },[])
    const login = async()=>{
        console.log(email,password);
        const res=await axios.post("http://localhost:3001/users/login",{email: email, password: password})
        if(res.data=="LOGIN FAILED"){
            alert("LOGIN FAILED")
        }
        else{
            router.push("/")
            localStorage.setItem("token",res.data.token);
            localStorage.setItem("name",res.data.user.name);
        }
    }
    return (
        <div>
            <div className="relative w-screen h-screen">
                <Image src="/login-bg.jpg" className="" layout="fill" objectFit="cover" />
            </div>
            <div className="absolute top-1/2 left-1/2 grid grid-cols-2 -translate-x-1/2 -translate-y-1/2 gap-x-10">
                <div>
                    <div className="flex gap-x-4">
                        <Image src="/icons8-dog.png" width={100} height={100} objectFit="contain" />
                        <h1 className="text-white text-8xl">PET</h1>

                    </div>
                    <h1 className="text-white text-8xl">TRAVEL</h1>
                </div>
                <div className="flex flex-col gap-y-3 bg-white p-5 rounded-md shadow-md">
                    <h1>Welcome to You!</h1>
                    <p>login to continue now</p>
                    <input value={email} onChange={(e)=>setemail(e.target.value)} placeholder="Email" className="border-2 p-2 rounded-md shadow-md"/>
                    <input type="password" value={password} onChange={(e)=>setpassword(e.target.value)} placeholder="password" className="border-2 p-2 rounded-md shadow-md"/>
                    <button onClick={login} className="text-white bg-black p-2 rounded-md">LOGIN NOW</button>
                    {/* <button className="flex items-center text-white bg-[#1877F2] rounded-md gap-x-6">
                        <Image src="/facebook.png" width={50} height={50} objectFit="contain" />
                        <p>LOGIN WITE FACEBOOK</p>
                    </button> */}
                    <a href="/register" className="hover:text-blue-600">REGISTER

                    </a>
                </div>
            </div>
        </div>
    )
}