import React, { useEffect, useState } from 'react'
import Navbar from "../components/Navbar";
import NavbarPlaceOwner from "../components/Navbar_place_owner";
import Footer from "../components/Footer";
import Image from "next/image";
import Link from "next/link";
import { MOCK_PLACES, MOCK_LOCATIONS } from "../mock/location";
import { MOCK_USER_BOOKMARKS } from "../mock/user";
import axios from 'axios'



export default function profile() {
    const [nav, setNav] = useState(false)

    const [places, setPlaces] = useState([])

    const handleDelete = (id) => {
        setPlaces((prev) => {
            return prev.filter((e) => e._id !== id);
        });
        axios.delete(`http://localhost:3001/places/${id}`)
    };
    useEffect(() => {
        axios.get(`http://localhost:3001/users/me`, {
            headers: {
                authorization: `Bearer ${localStorage.getItem('token')}`,
            },
        }).then((res) => {
            
            if (res.data.role == "place_owner") {
                setNav(true)
            } else { setNav(false) }
        })

        axios.get(`http://localhost:3001/places/my_places`, {
            headers: {
                authorization: `Bearer ${localStorage.getItem("token")}`
            }
        }).then(res => {
            setPlaces(res.data)
        })
    }, [])



    return (
        <div>
            {nav ? <NavbarPlaceOwner /> : <Navbar />}
            <div>
                <div>
                    <div className="flex p-14">
                        <div className="w-8/12 space-y-6 text-xl">
                            <h1>สถานที่ที่เพิ่มแล้ว</h1>
                            {/* <h1>Username : {loginData()}</h1>
                            <p>Nickname : </p>
                            <p>Email : </p>
                            <p>Phone : </p> */}
                            {/* {data.map((e) => {
                                return <div>
                                    <div><h1>{e.name}</h1></div>
                                    <div>{e.nickname}</div>
                                    <div>{e.email}</div>
                                    <div>{e.phonenumber}</div>
                                </div>
                            })} */}
                        </div>
                    </div>
                    <div className="space-y-5 p-14 relative">
                        {places.map((e) => (
                            <div
                                key={e.id}
                                className="bg-app-blue-light grid grid-cols-2 rounded-lg p-6 gap-x-24 relative cursor-pointer"
                            >
                                <div className="relative h-72 w-full overflow-hidden rounded-lg shadow-lg">
                                    <Image
                                        loader={({ src }) => src}
                                        src={e.img}
                                        layout="fill"
                                        objectFit="cover"
                                    />
                                </div>

                                <div className="flex flex-col gap-y-3">
                                    <Link href={`/location/${e.locationId._id}/${e._id}`}>
                                        <h1 className="font-bold">{e.name}</h1>
                                    </Link>
                                    <p>{e.detail}</p>
                                    <div className="flex space-x-3">
                                        <Image src="/location.svg" width={24} height={24} />
                                        <p>
                                            {e.locationId.name}, {e.district}, ประเทศไทย
                                        </p>
                                    </div>
                                    <div className="flex space-x-3">
                                        <Image src="/pet.svg" width={24} height={24} />
                                        <p>สัตว์สามารถเข้าได้</p>
                                    </div>
                                </div>
                                <div className="flex absolute bottom-3 right-6 space-x-6">
                                    <Link href={`/edit/${e._id}`}>
                                        <a className="">
                                            <Image src="/pen.png" width={64} height={64} />
                                        </a>
                                    </Link>
                                    <button onClick={() => handleDelete(e._id)} className="">
                                        <Image src="/delete 2.png" width={64} height={64} />
                                    </button>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    );
}