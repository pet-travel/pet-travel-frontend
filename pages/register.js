import Image from "next/image"
import React, { useState, useEffect } from 'react';
import axios from "axios";
import { useRouter } from "next/router";

export default function register() {
    const [name, setname] = useState("");
    const [email, setemail] = useState("");
    const [password, setpassword] = useState("");
    const [role, setrole] = useState("");
    const router = useRouter()
    
    useEffect(() => {
        if (localStorage.getItem("token")) { router.push("/") }
    }, [])
    const register = async () => {
        console.log(email, password);
        const res = await axios.post("http://localhost:3001/users/register", { name: name, email: email, password: password ,role: role})
        if (res.data == "REGISTER FAILED") {
            alert("REGISTER FAILED")
        }
        else {
            router.push("/login")
        }
    }


    return (
        <div>
            <div className="relative w-screen h-screen">
                <Image src="/login-bg.jpg" className="" layout="fill" objectFit="cover" />
            </div>
            <div className="absolute top-1/2 left-1/2 grid grid-cols-2 -translate-x-1/2 -translate-y-1/2 gap-x-10">
                <div>
                    <div className="flex gap-x-4">
                        <Image src="/icons8-dog.png" width={100} height={100} objectFit="contain" />
                        <h1 className="text-white text-8xl">PET</h1>

                    </div>
                    <h1 className="text-white text-8xl">TRAVEL</h1>
                </div>
                <div className="flex flex-col gap-y-3 bg-white p-5 rounded-md shadow-md">
                    <h1>Welcome!</h1>
                    <p>Sign up or log in to continue</p>
                    {/* <button className="flex items-center text-white bg-[#1877F2] rounded-md gap-x-6">
                        <Image src="/facebook.png" width={50} height={50} objectFit="contain" />
                        <p>LOGIN WITE FACEBOOK</p>
                    </button> */}
                    {/* <div className="flex flex-col items-center text-gray-400">
                        <t1>or</t1>
                    </div> */}
                    <input value={name} onChange={(e) => setname(e.target.value)} placeholder="Username" className="border-2 p-2 rounded-md shadow-md" />
                    <input value={email} onChange={(e) => setemail(e.target.value)} placeholder="Email" className="border-2 p-2 rounded-md shadow-md" />
                    <input type="password" value={password} onChange={(e) => setpassword(e.target.value)} placeholder="password" className="border-2 p-2 rounded-md shadow-md" />
                    <div className="flex justify-between ">
                        <input type="radio" value="user" onChange={(e) => setrole(e.target.value)} name="gender" />User
                        <input type="radio" value="place_owner" onChange={(e) => setrole(e.target.value) }name="gender" />Place Owner
                    </div>
                    <button onClick={register} className="text-white bg-black p-2 rounded-md">REGISTER</button>
                    <a href="/login" className="hover:text-blue-600">LOGIN
                    </a>
                </div>
            </div>
        </div>
    )
}