import React, { useState, useEffect } from "react";
import { MOCK_PLACES, MOCK_LOCATIONS } from "../../../../mock/location";
import { useRouter } from "next/router";
import Image from "next/image";
import Navbar from "../../../../components/Navbar";
import Footer from "../../../../components/Footer";
import axios from 'axios'
import NavbarPlaceOwner from "../../../../components/Navbar_place_owner";

const index = () => {
  const [place, setPlace] = useState({})
  const router = useRouter();
  useEffect(() => {
    if (router.query.placeId)
      axios.get(`http://localhost:3001/places/${router.query.placeId}`).then((res) => {
        console.log(res.data);
        setPlace(res.data)
      })

  }, [router.query.placeId])


  const bookmark = () => {
    axios.post('http://localhost:3001/bookmarks', {
      placeId: router.query.placeId
    }, {
      headers: {
        authorization: `Bearer ${localStorage.getItem("token")}`
      }
    }).then((res) => {
      router.push("/profile")
    })
  }

  const [nav, setNav] = useState(false)

  useEffect(() => {
    axios.get(`http://localhost:3001/users/me`, {
      headers: {
        authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    }).then((res) => {
      // console.log(res.data);
      if (res.data.role == "place_owner") {
        setNav(true)
      } else { setNav(false) }
    })
  },[])

  const [imgPreview, setImgPreview] = useState("/");

  return (
    <div>
       {nav ? <NavbarPlaceOwner /> : <Navbar />}
      <div className="grid grid-cols-2 px-24 py-12 gap-x-12">
        <div>
          <div>
            <div className="relative w-full h-96 shadow-lg rounded-lg overflow-hidden">
              <Image
                src={place.img || "/icons8-dog.png"} p
                loader={({ src }) => src}
                layout="fill"
                objectFit="cover"
                alt=""
              />
            </div>
            <div className="mt-12 flex gap-x-3">
            </div>
          </div>
        </div>
        <div className="flex flex-col gap-y-3 relative">
          <h1 className="font-bold">{place.name}</h1>
          <p>{place.detail}</p>
          <div className="flex space-x-3 mt-12">
            <Image src="/location.svg" width={24} height={24} />
            <p>
              {place.district}, ประเทศไทย
            </p>
          </div>
          <button onClick={bookmark} className="absolute bottom-3 right-6">
            <Image src="/hearth-red.svg" width={64} height={64} />
          </button>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default index;
