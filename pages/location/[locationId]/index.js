import React,{ useState , useEffect} from "react";
import Navbar from "../../../components/Navbar";
import Footer from "../../../components/Footer";
import Image from "next/image";
import { useRouter } from "next/router";
import { MOCK_LOCATIONS, MOCK_PLACES } from "../../../mock/location";
import Link from "next/link"
import axios from 'axios'
import NavbarPlaceOwner from "../../../components/Navbar_place_owner";
const index = () => {
  const [location, setLocation] = useState({})
  const router = useRouter()
  const [nav, setNav] = useState(false)

  useEffect(() => {
    axios.get(`http://localhost:3001/users/me`, {
      headers: {
        authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    }).then((res) => {
      // console.log(res.data);
      if (res.data.role == "place_owner") {
        setNav(true)
      } else { setNav(false) }
    })
  },[])

  useEffect(() => {
    if(router.query.locationId){
      axios.get(`http://localhost:3001/locations/${router.query.locationId}`).then((res) => {
        console.log(res.data);
        setLocation(res.data[0])
      })
    }
  }, [router.query.locationId])

  return (
    <div>
      {nav ? <NavbarPlaceOwner /> : <Navbar />}
      <div className="relative">
        <div className="relative w-full h-[70vh]">
          <h1 className="absolute top-12 inset-x-0 z-20 text-center text-white font-bold">
            สถานที่เที่ยวในจังหวัด {location.name}
          </h1>
          <div className="absolute top-0 right-0 bg-white z-10 w-full h-full opacity-20"></div>
          <Image src="/Dog.jpg" layout="fill" objectFit="cover" />
        </div>
        <div className="px-24 py-12">
          <h1 className="font-bold">{location.name}</h1>
          <ul className="list-disc pl-12 my-6">
            <li>
              <h3>สถานที่สัตว์เข้าได้</h3>
            </li>
          </ul>

          <div className="space-y-6">
            {location.places?.map((e) => (
              <Link key={e.id} href={`/location/${e.locationId}/${e._id}`}>
                <div className="bg-app-blue-light grid grid-cols-2 rounded-lg p-6 gap-x-24 relative cursor-pointer">
                  <div className="relative h-72 w-full overflow-hidden rounded-lg shadow-lg">
                    <Image
                      loader={({ src }) => src}
                      src={e.img}
                      layout="fill"
                      objectFit="cover"
                    />
                  </div>

                  <div className="flex flex-col gap-y-3">
                    <h1 className="font-bold">{e.name}</h1>
                    <p>{e.detail}</p>
                    <div className="flex space-x-3">
                      <Image src="/location.svg" width={24} height={24} />
                      <p>
                        {e.name}, {e.district}, ประเทศไทย
                      </p>
                    </div>
                    <div className="flex space-x-3">
                      <Image src="/pet.svg" width={24} height={24} />
                      <p>สัตว์สามารถเข้าได้</p>
                    </div>
                  </div>
                  {/* <button className="absolute bottom-3 right-6">
                    <Image src="/hearth-red.svg" width={64} height={64} />
                  </button> */}
                </div>
              </Link>
            ))}
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default index;
