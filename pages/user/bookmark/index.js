import React, { useState } from "react";
import Navbar from "../../../components/Navbar";
import Footer from "../../../components/Footer";
import Image from "next/image";
import { MOCK_PLACES, MOCK_LOCATIONS } from "../../../mock/location";
import { MOCK_USER_BOOKMARKS } from "../../../mock/user";
import Link from "next/link";

const index = () => {
  const [bookmarks, setBookmark] = useState(
    MOCK_USER_BOOKMARKS.map((e) => ({
      ...e,
      place: MOCK_PLACES.find((p) => e.placeId === p.id),
    }))
  );

  const handleDelete = (id) => {
    setBookmark((prev) => {
      return prev.filter((e) => e.id !== id);
    });
  };

  return (
    <div>
      <Navbar />
      <div className="px-24 py-12">
        <div className="flex space-x-3">
          <Image src="/location.svg" width={24} height={24} />
          <h1>สถานที่ที่ถูกใจแล้ว</h1>
        </div>
        <div className="space-y-12 mt-12">
          {bookmarks.map((e) => (
            <div
              key={e.id}
              className="bg-app-blue-light grid grid-cols-2 rounded-lg p-6 gap-x-24 relative cursor-pointer"
            >
              <div className="relative h-72 w-full overflow-hidden rounded-lg shadow-lg ">
                <Image className="z-0"
                  loader={({ src }) => src}
                  src={e.place.img}
                  layout="fill"
                  objectFit="cover"
                />
              </div>

              <div className="flex flex-col gap-y-3">
                <Link href={`/location/${e.place.locationId}/${e.place.id}`}>
                  <h1 className="font-bold">{e.place.name}</h1>
                </Link>
                <p>{e.place.detail}</p>
                <div className="flex space-x-3">
                  <Image src="/location.svg" width={24} height={24} />
                  <p>
                    {e.place.name}, {e.place.district}, ประเทศไทย
                  </p>
                </div>
                <div className="flex space-x-3">
                  <Image src="/pet.svg" width={24} height={24} />
                  <p>สัตว์สามารถเข้าได้</p>
                </div>
              </div>
              <div className="flex absolute bottom-3 right-6 space-x-6">
                <button className="">
                  <Image src="/hearth-red.svg" width={64} height={64} />
                </button>
                <button onClick={() => handleDelete(e.id)} className="">
                  <Image src="/delete 2.png" width={64} height={64} />
                </button>
              </div>
            </div>
          ))}
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default index;
